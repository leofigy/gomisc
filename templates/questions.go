package main

import (
    "net/http"
    "html/template"
    "io/ioutil"
    "io"
    "strings"
    "fmt"
)

var front = `
<!DOCTYPE html>
<html>
   <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
              
        <title>
            Ask me?
        </title>
    </head>
       
    <body>
       
    <section class="section">
    <div class="container">
      <h1 class="title">
          Best place to eat
      </h1>
      <p class="subtitle">
        List of topics to discuss and get your <strong>inputs</strong>!
        
      <form action="/alldone">

          
          {{range .Questions}}
          
          <div class="field">
            <div class="control">
                <label class=label>{{.ID}} - {{.Text}}</label>
                <textarea text name={{.ID}} placeholder="Tell us" rows="3" class="textarea is-primary"></textarea>
            </div>
          </div>
          
          {{end}}
          
      </p>
        
        <div class="control">
          <button class="button is-link" formmethod="post"> Save </button>
      </form>
    </div>
        
    </div>
    </section>
        
    </body>

</html>
`

var questions = `What is your favorite food?
What is the best beer in the Bay Area?`

type Survey struct {
    Questions []*Question 
}

type Question struct {
    ID int
    Text string 
}

func parseQuestions(inputs io.Reader) (*Survey, error) {
    questionaire := &Survey{}
    data, err := ioutil.ReadAll(inputs); if err != nil {
        return nil, err
    }
    
    lines := strings.Split(string(data), "\n")
    
    questions := make([]*Question,len(lines))
    
    for i := range lines {
        questions[i] = &Question{i+ 1, lines[i]}
    }

    questionaire.Questions = questions 
    
    return questionaire, nil 
} 

func Render(w http.ResponseWriter, r *http.Request) {
    t := template.New("ui")
    t, err := t.Parse(front); if err != nil {
        fmt.Fprintf(w, fmt.Sprintf("Fatal Error %v", err))
        return 
    }
    
    survey, err := parseQuestions(strings.NewReader(questions)); if err != nil {
        fmt.Fprintf(w, fmt.Sprintf("Fatal Error %v", err))
        return 
    }

    t.Execute(w, survey)
}

func ParamsSaver(w http.ResponseWriter, r *http.Request){

    err := r.ParseForm()
	if err != nil {
		panic(err)
	}

    // just print 
    
    for i := range r.Form {
        fmt.Println(r.Form.Get(i))
    }
    
    fmt.Fprintf(w, "..... sent ")
}

func main(){
    
    server := http.Server {
        Addr: "127.0.0.1:5001",
    }
    
    http.HandleFunc("/", Render)
    http.HandleFunc("/alldone", ParamsSaver)
    server.ListenAndServe()
}